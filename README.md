# This is a fork

This is a fork of [emacs-pe/crontab-mode](https://github.com/emacs-pe/crontab-mode).  


## What does this better?

The original crontab-mode couldn't match ranges or lists of weekdays given as names. This one does it.  

```crontab
1 * * * mon-fri command
1 * * * mon,tue,thu command
```

![img](assets/This_is_a_fork/2024-05-01T09.54.56-screenshot.png)
